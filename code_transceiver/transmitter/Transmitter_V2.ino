#include <RH_ASK.h>
#include <SPI.h>


RH_ASK rf_driver;

struct Signal {
  int throttle;
  short rudder;
  int pump;
};

typedef struct Signal Data;
Data data;

void setup(){
  Serial.begin(9600);
  data.throttle = 530;
  data.rudder = 127;
  data.pump = 530;
  rf_driver.init();
  
}

void loop(){
  rf_driver.send((uint8_t *)&data, sizeof(data));
  rf_driver.waitPacketSent();
  Serial.println("Successful transmission");
}
 
