#include <Servo.h>

const int FM1 = 3;
const int BM1 = 5;
const int FM2 = 6;
const int BM2 = 9;
const int S = 7;

Servo servo_p7;

void Turn(int valueX){
  int angle = map(valueX, 1023, 0, 0, 180);
  if(angle == 89 || angle == 90 || angle == 91){
    servo_p7.write(90);
  }
  else {
    servo_p7.write(angle);
  }
}
void Motor(int valueY){
  int pwm = map(valueY, 0, 1023, 0, 255);
  if(pwm < 126){
    analogWrite(BM1, pwm);
    analogWrite(BM2, pwm);
    analogWrite(FM1, 0);
    analogWrite(FM2, 0);
  }
  else if(pwm > 129){
    analogWrite(FM1, pwm);
    analogWrite(FM2, pwm);
    analogWrite(BM1, 0);
    analogWrite(BM2, 0);
  }
  else{
    analogWrite(BM1, 0);
    analogWrite(BM2, 0);
    analogWrite(FM1, 0);
    analogWrite(FM2, 0);
  }
}

void setup() {
  // put your setup code here, to run once:
  servo_p7.attach(7);
  servo_p7.write(90);

  pinMode(FM1, OUTPUT);
  pinMode(BM1, OUTPUT);
  pinMode(FM2, OUTPUT);
  pinMode(BM2, OUTPUT);

}

void loop() {
  // put your main code here, to run repeatedly:
  Turn(analogRead(A0));

  Motor(analogRead(A1));
}
